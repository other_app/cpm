$(function() {

    $('.number-only').number(true, 2);

    var config = {
        autoclose: true,
        language: 'kh'
    };

    var picker = $('#datetime_picker');

    function switchPicker() {
        var _picker = $('#datetime_picker');
        var _date = $('#selDate').val();
        $(_picker).datepicker('remove');
        switch (_date) {
            case 'daily':
                config.minViewMode = 0;
                config.format = "yyyy-mm-dd";
                break;

            case 'weekly':
                config.minViewMode = 0;
                config.calendarWeeks = true;
                config.format = "dd-mm-yyyy";
                break;

            case 'monthly':
                config.minViewMode = 1;
                config.format = "mm-yyyy";
                break;

            case 'yearly':
                config.minViewMode = 2;
                config.format = "yyyy";
                break;
        }

        $(_picker).datepicker(config).on('change', function() {
            var date = $('input[name=date]').val();
            $('input[name=date]').val('');
            $('input[name=date]').val(date);
        });
    }

    switchPicker();

    $('#selDate').click(function() {
        $('input[name=date]').val('');
        switchPicker();
    });

    $('input[name=date]').change(function() {
        var type = $('#selDate').val();
        var date = $(this).val();
        if (type == 'weekly') {
            $(this).val(moment(date, "DD/MM/YYYY").week());
        }
    });

    var mydata = {};
    $('#tblBudget').hide();
    // $('#frmBudget').submit(function(e) {
    //     e.preventDefault();
    //     var link = $(this).attr('action');
    //     var form = $(this).serialize();

    //     $.get(link, form, function(data){
    //         console.log(data);
    //         mydata = data;
    //         $('#tblBudget​​').listData(data);
    //         $('#tblBudget​​').fadeOut('slow', function() {
    //             $(this).fadeIn('slow');
    //         });
    //     });
    // });

    $(document).on('click', '.btnModify', function(){
        var id = $(this).parents('tr').attr('id');
        window.location.href = 'http://localhost:8000/dash/budget/' + $('#selBudgetType').val() + '/' + id + '/edit';
    });

    $(document).on('click', '.btnApprove', function(){
        var id = $(this).parents('tr').attr('id');
        window.location.href = 'http://localhost:8000/dash/budget/' + $('#selBudgetType').val() + '/' + id + '/edit';
    });

}); // end document ready
