$(function() {

    $('select').change(function() {
        $('#data').html('');
    });

    $('#btnShowPrint').click(function() {
        console.log(record);

        var date = $('#cmbDay').val();
        var type = $('#cmbTypeCash').val();
        var title = $('#lblministry').text();
        if (type == 1) {
            $('#data').load('/views/print_income.php', {}, function() {
                $('#headTitle').text('ផែនការចំណូលរបស់ក្រសួង ស្ថាប័ន ' + title + ' តាមជំពូក');
                $('#timeCol').text(date);
                $(document).find('.ch_70').text($('#up_ch_70').text());
                $(document).find('.ch_71').text($('#up_ch_71').text());
                $(document).find('.ch_72').text($('#up_ch_72').text());
                $(document).find('.ch_73').text($('#up_ch_73').text());
                $(document).find('.ch_74').text($('#up_ch_74').text());
                $(document).find('.ch_75').text($('#up_ch_75').text());
                $(document).find('.ch_76').text($('#up_ch_76').text());
                $(document).find('.ch_77').text($('#up_ch_77').text());
                $(document).find('.ch_78').text($('#up_ch_78').text());
                $(document).find('.ch_27').text($('#up_ch_27').text());
                $(document).find('.ch_50').text($('#up_ch_50').text());
            });
        } else {
            $('#data').load('/views/print_expense.php', {}, function() {
                $('#headTitle').text('ផែនការចំណាយរបស់ក្រសួង ស្ថាប័ន ' + title + ' តាមជំពូក');
                $('#timeCol').text(date);
                $(document).find('.ch_60').text($('#up_ch_60').text());
                $(document).find('.ch_61').text($('#up_ch_61').text());
                $(document).find('.ch_64').text($('#up_ch_64').text());
                $(document).find('.ch_66').text($('#up_ch_66').text());
                $(document).find('.ch_62').text($('#up_ch_62').text());
                $(document).find('.ch_65').text($('#up_ch_65').text());
                $(document).find('.ch_63').text($('#up_ch_63').text());
                $(document).find('.ch_67').text($('#up_ch_67').text());
                $(document).find('.ch_68').text($('#up_ch_68').text());
                $(document).find('.ch_69').text($('#up_ch_69').text());
                $(document).find('.ch_50').text($('#up_ch_50').text());
                $(document).find('.ch_20').text($('#up_ch_20').text());
                $(document).find('.ch_21').text($('#up_ch_21').text());
                $(document).find('.ch_22').text($('#up_ch_22').text());
                $(document).find('.ch_23').text($('#up_ch_23').text());
                $(document).find('.ch_26').text($('#up_ch_26').text());
                $(document).find('.ch_27').text($('#up_ch_27').text());
            });
        }
    });

    $(document).on('click', '#btnPrint', function() {
        window.print();
    });

});
