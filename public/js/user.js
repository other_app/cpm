$(function() {
    var buttons = '<span class="text-center"><button class="btn btn-info btn-xs btnModify" data-title="Modify"><span class="glyphicon glyphicon-pencil"></span></button>';
            // buttons += ' <button class="btn btn-danger btn-xs btnDelete" data-title="Delete"><span class="glyphicon glyphicon-trash"></span></button></span>';

    var table = $('#tblUsers').DataTable({
        ajax: {
            url: _link +  'dash/user/json',
            dataSrc: ''
        },
        columns: [
            { data: 'username', name: 'username' },
            { data: 'firstname', name: 'firstname' },
            { data: 'lastname', name: 'lastname' },
            { data: 'email', name: 'email' },
            { data: 'gender', name: 'gender' },
            { data: 'date_of_birth', name: 'date_of_birth' },
            { data: '', name: '' },
        ],
        columnDefs: [
            {
                targets: -1, data: null, defaultContent: buttons
            }
        ],
    });

    $(document).on('click', '#tblUsers .btnModify', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        window.location.href = _link + 'dash/user/' + id + '/edit';
    });

    $(document).on('click', '#tblUsers .btnDelete', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        if (confirm('Do you really want to delete this user?')) {
            $.post(_link + 'dash/user/' + id, {'_method':'delete', '_token':_code}, function(result){
                table.row(tr).node().remove();
            }, 'json');
        }
    });

    $(document).on('click', '#tblUsers td:not(td:has(.btnModify))', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        window.location.href = _link + 'dash/user/' + id;
    });

});
