(function ( $ ) {

    // ajax configuration
    $.ajaxSetup({async:false});

    // datapicker khmer configuration
    $.fn.datepicker.dates['kh'] = {
        days: ["ថ្ងៃអាទិត្យ", "ថ្ងៃចន្ទ", "ថ្ងៃអង្គារ", "ថ្ងៃពុធ", "ថ្ងៃព្រហស្បតិ៍", "ថ្ងៃសុក្រ", "ថ្ងៃសៅរ៍"],
        daysShort: ["អាទិត្យ", "ចន្ទ", "អង្គារ", "ពុធ", "ព្រហស្បតិ៍", "សុក្រ", "សៅរ៍"],
        daysMin: ["អា", "ច", "អ", "ពុ", "ព្រ", "សុ", "ស"],
        months: ["ខែមករា", "ខែកុម្ភៈ", "ខែមិនា", "ខែមេសា", "ខែឧសភា", "ខែមិថុនា", "ខែកក្កដា", "ខែសីហា", "ខែកញ្ញា", "ខែតុលា", "ខែវិច្ឆិកា", "ខែធ្នូ"],
        monthsShort: ["មករា", "កុម្ភៈ", "មិនា", "មេសា", "ឧសភា", "មិថុនា", "កក្កដា", "សីហា", "កញ្ញា", "តុលា", "វិច្ឆិកា", "ធ្នូ"],
        today: "ថ្ងៃនេះ",
        clear: "សំអាត",
        //format: "mm/dd/yyyy",
        format: "yyyy-mm-dd",
        titleFormat: "MM yyyy",
        weekStart: 0
    };

    // datatable khmer configuration
    $.extend( true, $.fn.dataTable.defaults, {
        "language": {
            "decimal":        "",
            "emptyTable":     "No data available in table",
            "info":           "បង្ហាញ _START_ ដល់​ _END_ នៃទិន្នន័យសរុបចំនួន _TOTAL_",
            "infoEmpty":      "បង្ហាញ 0 ដល់​ 0 នៃទិន្នន័យសរុបចំនួន 0",
            "infoFiltered":   "(ស្វែងរក​​ពីទិន្នន័យសរុបចំនួន _MAX_)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "បង្ហាញ _MENU_ ជួរដេកក្នុង១ទំព័រ",
            "loadingRecords": "កំពុងរៀបចំទិន្ន័យ...",
            "processing":     "កំពុងដំណើរការ...",
            "search":         "ស្វែងរក​​ ៖",
            "zeroRecords":    "មិនមានទិន្នន័យនេះទេ",
            "paginate": {
                "first":      "ទំព័រមុខ",
                "last":       "ទំព័រចុងក្រោយ",
                "next":       "បន្ទាប់",
                "previous":   "ត្រលប់"
            }
        }
    });

    $.mydate = ["មករា", "កុម្ភៈ", "មិនា", "មេសា", "ឧសភា", "មិថុនា", "កក្កដា", "សីហា", "កញ្ញា", "តុលា", "វិច្ឆិកា", "ធ្នូ"];

}( jQuery ));