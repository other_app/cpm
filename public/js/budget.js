(function ( $ ) {
    $('.data-inform').show();
    $('.data-containter').hide();

    $('#divBudget').show();
    $('#divReport').hide();

    var my = {};

    $(function() {
        var incomes = ['70', '71', '72', '73', '74', '75', '76', '77', '78', '27', '50'];
        var expenses = ['60', '61', '64', '66', '62', '65', '63', '67', '68', '69', '50', '20', '21', '22', '23', '26', '27'];

        var link = $(document.body).data('link'),
            budgets = {};

        var config = {
            autoclose: true,
            language: 'kh'
        };

        my.switchPicker();

        $('#selDate').click(function() {
            $('input[name=date]').val('');
            my.switchPicker();
        });

        $('input[name=date]').change(function() {
            var type = $('#selDate').val(), date = $(this).val();
            if (type == 'weekly') {
                $(this).val(moment(date, "DD/MM/YYYY").week());
            }
        });

        $('#frmBudget').submit(function(e) {
            e.preventDefault();

            if (!my.formValid()) {
                return;
            }

            var link = $(this).attr('action'),
                form = $(this).serialize();

            $.get(link, form, function(data){
                console.log(data);
                budgets = data.budgets;

                $('#tblBudget​​').listData(data);
                $('.number-only').number(true, 2);
                $('.data-inform').slideUp();
                $('.data-containter').slideDown();
            });
        });

        $(document).on('click', '.btnModify', function(){
            var id = $(this).parents('tr').attr('id'),
                type = $('#selBudgetType').val(),
                user = $('#cbxIsAdmin').is(':checked') ? 'admin' : 'user';
            window.location.href = link + '/dash/budget/' + type + '/' + id + '/edit' + '?user=' + user;
        });

        $(document).on('click', '.btnApprove', function(){
            var id = $(this).parents('tr').attr('id'),
                type = $('#selBudgetType').val();
            window.location.href = link + '/dash/budget/approve/' + type + '/' + id;
        });

        $(document).on('click', '#btnPrint', function(){
            window.print();
        });

        $(document).on('click', '#btnHideReport', function(){
            $('#divBudget').fadeIn('400', function() {
                $('#divReport').fadeOut();
            });
        });

        $(document).on('click', '.btnReport', function(){
            var budget_type, budget_link, header_title, header_date;
            var id = $(this).parents('tr').attr('id'),
                type = $('#selBudgetType').val(),
                dtype = $('#selDate').val(),
                date = $('#datetime_picker input[name=date]').val(),
                data = {};

            $.each(budgets, function(i,e){
                if (e.id == id) {
                    data = e;
                    return;
                }
            });

            if (dtype == 'daily') {
                var od = date;
                var dt = date.substring(5,7);
                var dd = $.mydate[parseInt(dt)-1];
                header_date = date.replace(dt, dd);
            }

            if (dtype == 'monthly') {
                var dt = date.substring(0,2);
                header_date = $.mydate[parseInt(dt)-1];
            }

            if (type == 'income') {
                header_title = 'ផែនការចំណូលរបស់ក្រសួង ' + data.ministry_nm + ' តាមជំពូក';
                budget_type = incomes;
                budget_link = '/dash/report_income';
            } else {
                header_title = 'ផែនការចំណាយរបស់ក្រសួង ' + data.ministry_nm + ' តាមជំពូក';
                budget_type = expenses;
                budget_link = '/dash/report_expense';
            }

            $('#divReport').load(link + budget_link, function(){
                var date = $('#datetime_picker input:text').val();
                $('#lblMinistry').text(header_title);
                $('#lblDate').text(header_date);
                $.each(budget_type, function(i,e){
                    $('#chapter_' + e).text(data['chapter_' + e]);
                });

                $('.number-only').number(true, 2);
            });

            $('#divBudget').hide();
            $('#divReport').show();
        });

    }); // end document ready

    $.fn.listData = function (data) {
        if (typeof data === 'undefined' || data === null || data === '' || data === 0) {
            alert('No data found in $.listData(data) !!!');
            return;
        }

        var tags = '', role = data.auth.role, budgets = data.budgets, records = (data.incomes) ? data.incomes : data.expenses;

        if (budgets.length > 0) {
            tags = '<table class="table table-bordered tbl"><thead><tr>';
            tags += '<th>ជំពូក</th><th>បរិយាយ</th><th>ចំនួនថវិកា</th><th>ច្បាប់ថវិកា</th>';
            tags += '<th>ឥណទានថ្មី</th><th>សរុបរួម</th><th>ចំនូល (%)</th></tr></thead>';
            $.each(budgets, function(i,item){
                tags += '<tbody data-id="' + item.ministry_id + '">';
                $.each(records, function(i,ch){
                    tags += '<tr><td>' + ch.kh_no + '</td>';
                    tags += '<td>' + ( (data.incomes) ? ch.income : ch.expense ) + '</td>';
                    tags += '<td class="number-only">' + item['chapter_' + ch.en_no] + '</td>';
                    tags += '<td></td><td></td><td></td><td></td></tr>';
                });

                tags += '<tr id="' + item.id + '"><th colspan="17">';
                tags += '<div class="row">';
                tags += '    <div class="col-md-6">';
                tags += '        <span>' + item.ministry_nm + '</span>';
                tags += '    </div>';
                tags += '    <div class="col-md-6"><div class="row">';
                tags += '        <div class="col-md-4 pull-right"><button class="btn btn-block btn-default btnReport">បង្ហាញរបាយការណ៏</button></div>';

                if (role == 'admin') {
                    if ($('#cbxIsAdmin').is(':checked')) {
                        tags += '        <div class="col-md-4 pull-right"><button class="btn btn-block btn-info btnModify">កែប្រែ</button></div>';
                    }
                    if (!$('#cbxIsTotal').is(':checked')) {
                        tags += '    <div class="col-md-4 pull-right"><button class="btn btn-block btn-primary btnApprove" ';
                        tags += (item.approved_id > 0 ? "disabled" : "") + '>ទទួលយក</button></div>';
                    }
                } else {
                    if (!$('#cbxIsTotal').is(':checked')) {
                        tags += '    <div class="col-md-4 pull-right"><button class="btn btn-block btn-info btnModify">កែប្រែ</button></div>';
                    }
                }

                tags += '    </div><div>';
                tags += '</div>';
                tags += '</th></tr>';
                tags += '</tbody>';
            });
            tags += '</table>';
        } else {
            tags = '<div class="alert alert-danger"><strong>សូមអភ័យទោស​! </strong>';
            tags += 'មិនមានទិន្នន័យសំរាប់​បង្ហាញទេ សូមជ្រើសរើសកាលបរិច្ឆេត្តសារជាថ្មី</div>';
        }

        this.html(tags);
        return this;
    }

    my.formValid = function () {
        var _date = $('#datetime_picker input[name=date]').val();
        if (_date == '' || _date == 'undefined') {
            alert('សូមជ្រើសរើសកាលបរិច្ឆេត្ត');
            return false;
        }
        return true;
    };

    my.switchPicker = function() {
        var _picker = $('#datetime_picker'),
            _date = $('#selDate').val(),
            _config = {
                autoclose: true,
                language: 'kh'
            };

        $(_picker).datepicker('remove');

        switch (_date) {
            case 'daily':
                _config.minViewMode = 0;
                _config.format = "yyyy-mm-dd";
                break;

            case 'weekly':
                _config.minViewMode = 0;
                _config.calendarWeeks = true;
                _config.format = "dd-mm-yyyy";
                break;

            case 'monthly':
                _config.minViewMode = 1;
                _config.format = "mm-yyyy";
                break;

            case 'yearly':
                _config.minViewMode = 2;
                _config.format = "yyyy";
                break;
        }

        $(_picker).datepicker(_config).on('change', function() {
            var date = $('input[name=date]').val();
            $('input[name=date]').val(date);
        });
    };


}( jQuery )); // 098 654 285