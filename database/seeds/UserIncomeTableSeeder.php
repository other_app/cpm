<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserIncomeTableSeeder extends Seeder {
    public function run() {
        $user = [1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5];
        for ($i = 1; $i <= 15; ++$i) {
            DB::table('user_incomes')->insert([
                'user_id'     => $user[$i - 1],
                'ministry_id' => $user[$i - 1],
                'chapter_70'  => $i,
                'chapter_71'  => $i,
                'chapter_72'  => $i,
                'chapter_73'  => $i,
                'chapter_74'  => $i,
                'chapter_75'  => $i,
                'chapter_76'  => $i,
                'chapter_77'  => $i,
                'chapter_78'  => $i,
                'chapter_27'  => $i,
                'chapter_50'  => $i,
                'approved_id' => 0,
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now()
            ]);
        }
    }
}
