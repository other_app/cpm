<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AdminIncomeTableSeeder extends Seeder {
    public function run() {
        for ($i = 1; $i <= 5; ++$i) {
            DB::table('admin_incomes')->insert([
                'user_id'     => $i,
                'ministry_id' => $i,
                'chapter_70'  => $i,
                'chapter_71'  => $i,
                'chapter_72'  => $i,
                'chapter_73'  => $i,
                'chapter_74'  => $i,
                'chapter_75'  => $i,
                'chapter_76'  => $i,
                'chapter_77'  => $i,
                'chapter_78'  => $i,
                'chapter_27'  => $i,
                'chapter_50'  => $i,
                'approved_id' => 0,
                'created_by'  => $i,
                'updated_by'  => $i,
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now()
            ]);
        }
    }
}
