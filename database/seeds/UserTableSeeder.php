<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {
    public function run() {
        $users = [
            [1, 'user1', '123123', 'mr', 'user1', 'user'],
            [2, 'user2', '123123', 'mr', 'user2', 'user'],
            [3, 'user3', '123123', 'mr', 'user3', 'user'],
            [4, 'user4', '123123', 'mr', 'user4', 'user'],
            [5, 'user5', '123123', 'mr', 'user5', 'user'],

            [1, 'hero', '123123', 'sim', 'virak', 'admin'],
            [1, 'maru', '123123', 'sai', 'vichet', 'admin'],
            [1, 'ravuthz', '123123', 'yo', 'vannaravuth', 'admin'],
            [1, 'superadmin', 'SuperAdmin123', 'Mr', 'Rocky', 'admin']
        ];

        foreach ($users as $user) {
            DB::table('users')->insert([
                'ministry_id'    => $user[0],
                'username'       => $user[1],
                'password'       => bcrypt($user[2]),
                'firstname'      => $user[3],
                'lastname'       => $user[4],
                'email'          => $user[1] . '@gmail.com',
                'gender'         => 'male',
                'role'           => $user[5],
                'status'         => 'on',
                'date_of_birth'  => date('Y-m-d H:i:s'),
                'remember_token' => '',
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s')
            ]);
        }
    }
}
