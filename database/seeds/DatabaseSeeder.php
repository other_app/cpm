<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {
    public function run() {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(MinistryTableSeeder::class);
        $this->call(LawBudgetTableSeeder::class);
        $this->call(UserIncomeTableSeeder::class);
        // $this->call(AdminIncomeTableSeeder::class);
        $this->call(UserExpenseTableSeeder::class);
        // $this->call(AdminExpenseTableSeeder::class);
        $this->call(ChapterTableSeeder::class);

        Model::reguard();
    }
}
