<?php

use Illuminate\Database\Seeder;

class MinistryTableSeeder extends Seeder {
    public function run() {
        $minstries = [
            ['ក្រសួងការពារជាតិ', '1', 1],
            ['ក្រសួងសេដ្ឋកិច្ច និងហិរញ្ញវត្ថុ', '1', 3],
            ['ក្រសួងការបរទេស និងសហប្រតិបត្តិការអន្តរជាតិ', '1', 5],
            ['ក្រសួងការងារ បណ្ដុះបណ្ដាលវិជ្ជាជីវៈ', '1', 1],
            ['ក្រសួងពាណិជ្ជកម្ម', '1', 3]
        ];

        foreach ($minstries as $ministry) {
            DB::table('ministries')->insert([
                'name'       => $ministry[0],
                'status'     => $ministry[1],
                'created_by' => $ministry[2],
                'updated_by' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
    }
}
