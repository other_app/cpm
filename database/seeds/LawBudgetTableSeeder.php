<?php

use Illuminate\Database\Seeder;

class LawBudgetTableSeeder extends Seeder
{
    public function run()
    {
        $budgets = array(
            array(12, 12, 1, 1),
            array(13, 21, 2, 1),
            array(10, 22, 2, 2),
            array(11, 32, 3, 2),
            array(19, 12, 4, 3),
            array(21, 32, 1, 3),
        );

        foreach ($budgets as $budget) {
            DB::table('law_budgets')->insert([
                'income' => $budget[0],
                'expense' => $budget[1],
                'user_id' => $budget[2],
                'ministry_id' => $budget[3],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
    }
}
