<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserExpenseTableSeeder extends Seeder {
    public function run() {
        $user = [1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5];
        for ($i = 1; $i <= 15; ++$i) {
            DB::table('user_expenses')->insert([
                'user_id'     => $user[$i - 1],
                'ministry_id' => $user[$i - 1],
                'approved_id' => 0,
                'chapter_60'  => $i,
                'chapter_61'  => $i,
                'chapter_62'  => $i,
                'chapter_63'  => $i,
                'chapter_64'  => $i,
                'chapter_65'  => $i,
                'chapter_66'  => $i,
                'chapter_67'  => $i,
                'chapter_68'  => $i,
                'chapter_69'  => $i,
                'chapter_20'  => $i,
                'chapter_21'  => $i,
                'chapter_22'  => $i,
                'chapter_23'  => $i,
                'chapter_26'  => $i,
                'chapter_27'  => $i,
                'chapter_50'  => $i,
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now()
            ]);
        }
    }
}
