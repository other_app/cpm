<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminIncomesTable extends Migration {
    public function up() {
        Schema::create('admin_incomes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('ministry_id')->unsigned();
            $table->double('chapter_70', 11, 2)->unsigned();
            $table->double('chapter_71', 11, 2)->unsigned();
            $table->double('chapter_72', 11, 2)->unsigned();
            $table->double('chapter_73', 11, 2)->unsigned();
            $table->double('chapter_74', 11, 2)->unsigned();
            $table->double('chapter_75', 11, 2)->unsigned();
            $table->double('chapter_76', 11, 2)->unsigned();
            $table->double('chapter_77', 11, 2)->unsigned();
            $table->double('chapter_78', 11, 2)->unsigned();
            $table->double('chapter_27', 11, 2)->unsigned();
            $table->double('chapter_50', 11, 2)->unsigned();
            $table->integer('approved_id')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('admin_incomes');
    }
}
