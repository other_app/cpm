<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserExpensesTable extends Migration {
    public function up() {
        Schema::create('user_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('ministry_id')->unsigned();
            $table->double('chapter_60', 11, 2)->unsigned();
            $table->double('chapter_61', 11, 2)->unsigned();
            $table->double('chapter_62', 11, 2)->unsigned();
            $table->double('chapter_63', 11, 2)->unsigned();
            $table->double('chapter_64', 11, 2)->unsigned();
            $table->double('chapter_65', 11, 2)->unsigned();
            $table->double('chapter_66', 11, 2)->unsigned();
            $table->double('chapter_67', 11, 2)->unsigned();
            $table->double('chapter_68', 11, 2)->unsigned();
            $table->double('chapter_69', 11, 2)->unsigned();
            $table->double('chapter_20', 11, 2)->unsigned();
            $table->double('chapter_21', 11, 2)->unsigned();
            $table->double('chapter_22', 11, 2)->unsigned();
            $table->double('chapter_23', 11, 2)->unsigned();
            $table->double('chapter_26', 11, 2)->unsigned();
            $table->double('chapter_27', 11, 2)->unsigned();
            $table->double('chapter_50', 11, 2)->unsigned();
            $table->timestamps();
            $table->integer('approved_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down() {
        Schema::drop('user_expenses');
    }
}
