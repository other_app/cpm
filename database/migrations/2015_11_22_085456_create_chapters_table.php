<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChaptersTable extends Migration
{
    public function up()
    {
        Schema::create('chapters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('en_no')->unsigned();
            $table->string('kh_no', 4);
            $table->text('income');
            $table->text('expense');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('chapters');
    }
}
