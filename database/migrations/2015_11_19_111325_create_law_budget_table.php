<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLawBudgetTable extends Migration
{
    public function up()
    {
        Schema::create('law_budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('ministry_id')->unsigned();
            $table->integer('income')->unsigned();
            $table->integer('expense')->unsigned();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('law_budgets');
    }
}
