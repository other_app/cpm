<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ministry_id')->unsigned();
            $table->string('username');
            $table->string('password', 60);
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->enum('gender', ['male', 'female']);
            $table->enum('role', ['admin', 'user']);
            $table->tinyInteger('status');
            $table->timestamp('date_of_birth');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('users');
    }
}
