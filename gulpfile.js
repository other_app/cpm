var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass([
        'virak/style.scss'
    ], 'public/css/virak/style.css');

    mix.scripts([
        'virak/pubsub.js',
        'virak/ajax-helpers.js',
        'virak/ministry.js'
    ], 'public/js/customs/ministry.js');
});
