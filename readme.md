# **CPM Project**

We create this team to work as group with our source code in BitBucket with Laravel Framework, Start on Thursday, 19, November, 2015

**Members**:

* Sim Vireak
* Sai Vichet
* Yo Vannaravuth

> First commit is empty laravel 5.1 project

### Version
1.0.0

## Command create migration, seeder and model
```
#!php
php artisan make:migration create_ministries_table --create="ministries"
php artisan make:migration create_user_incomes_table --create="user_incomes"
php artisan make:migration create_user_expenses_table --create="user_expenses"
php artisan make:migration create_admin_incomes_table --create="admin_incomes"
php artisan make:migration create_admin_expenses_table --create="admin_expenses"
php artisan make:migration create_law_budget_table --create="law_budgets"
php artisan make:migration create_chapters_table --create="chapters"

php artisan make:seeder UserTableSeeder
php artisan make:seeder MinistryTableSeeder
php artisan make:seeder UserIncomeTableSeeder
php artisan make:seeder UserExpenseTableSeeder
php artisan make:seeder AdminIncomeTableSeeder
php artisan make:seeder AdminExpenseTableSeeder
php artisan make:seeder LawBudgetTableSeeder
php artisan make:seeder ChapterTableSeeder

php artisan make:model User
php artisan make:model Ministry
php artisan make:model UserIncome
php artisan make:model UserExpense
php artisan make:model AdminIncome
php artisan make:model AdminExpense
php artisan make:model LawBudget
php artisan make:model Chapter

php artisan make:model User -m
php artisan make:model User --migration

php artisan make:controller UserController
php artisan make:controller MinistryController
php artisan make:controller DashboardController
```

### Git Remember Username and Password:
git config --global credential.helper wincred