(function () {
    var submitAjaxRequest = function (e) {
        e.preventDefault();

        var form = $(this);

        var method = form.find('input[name="_method"]').val() || form.prop('method');
        
        $.ajax({
            type: method,
            url: form.prop('action'),
            data: form.serialize(),
            success: function (data) {
                
                $.publish('form.submitted', { form, data } );

            },
            error: function (data) {
                
                $.publish('form.submitted', { form, data } );

            }
        });

    }

    // Form marked with the "data-remote" attribute will submit, via AJAX
    $(document).on('submit', 'form[data-remote]', submitAjaxRequest);

})();