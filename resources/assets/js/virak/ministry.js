(function () {

    // Form Request via AJAX
    $.subscribe('form.submitted', function (event, data) {

        var formID = data.form.prop('id');

        if( formID === 'ministryIdEditBtn' ) {

            var fakeFormAction = $('#formMinistry').prop('action') + '/' + data.data.id;

            var status = data.data.status == 1 ? 'on' : 'off';

            $('#formMinistry').prop('action', fakeFormAction);

            $('#formMinistry').prepend('<input type="hidden" name="_method" value="PATCH" />');

            $('#formMinistry #name').val( data.data.name );
        
            $('#formMinistry input[name=status][value="' + status + '"]').prop('checked', true);

            $('#formValidation .alert.alert-success').hide();
            
            $('#btnMinistry').prop('value', 'កែប្រែ');

            $('#modalMinistry').modal('show');

        } else {

            if( data.data.status == 422 ){

                var errors = $.parseJSON(data.data.responseText);

                var errorsMessageList = '';

                $.each(errors, function(index, value) {
                    errorsMessageList += "<li>" + value + "</li>";
                });
            
                $('#formValidation .alert.alert-danger').empty().append( '<ul>' + errorsMessageList + '</ul>' ).fadeIn(500);

            } else {

                $('#formValidation .alert.alert-danger').hide();
                
                var btnMinistry = $('#btnMinistry');
                
                btnMinistry.prop('disabled', true);
                
                if( btnMinistry.val() === 'បញ្ចូល' ) {
                    $('#formValidation .alert.alert-success').text("បានបន្ថែមដោយជោគជ័យ!").fadeIn(500).delay(1000).fadeOut(500);

                } else {
                    $('#formValidation .alert.alert-success').text("បានកែប្រែដោយជោគជ័យ!").fadeIn(500).delay(1000).fadeOut(500);
                }
                location.href = '/dash/ministry';
                
            }
        }
    });

    // Clear Add New Ministry Form
    $("#modalMinistry").on('hidden.bs.modal', function () {
        
        $('#btnMinistry').prop('disabled', false);

        $('#btnMinistry').prop('value', 'បញ្ចូល');

        $('#formMinistry').find('input[name="_method"]').remove();

        $('#formValidation .alert.alert-danger').hide();

        $('#formValidation .alert.alert-success').hide();
        
        $('#formMinistry').prop('action', '');
        
        $('#formMinistry').trigger('reset');

    });

})();