@extends('auth')

@section('content')

<div class="row">
    <div class="col-md-12">

        <form method="POST" action="/register">
            {!! csrf_field() !!}

            <div class="form-group">
                <div class="col-md-6">
                    <label for="username">Username</label>
                    <input class="form-control" type="text" name="username">
                </div>
                <div class="col-md-6">
                    <label for="date_of_birth">Date of Birth</label>
                    <input class="form-control" type="text" name="date_of_birth">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6">
                    <label for="password">Password</label>
                    <input class="form-control" type="text" name="password">
                </div>
                <div class="col-md-6">
                    <label for="wordpass">Confirm Password</label>
                    <input class="form-control" type="text" name="wordpass">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6">
                    <label for="firtstname">Firstname</label>
                    <input class="form-control" type="text" name="firtstname">
                </div>
                <div class="col-md-6">
                    <label for="lastname">Lastname</label>
                    <input class="form-control" type="text" name="lastname">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-4">
                    <label for="username">Role</label>
                    <input class="form-control" type="text" name="role">
                </div>
                <div class="col-md-4">
                    <label for="username">Status</label>
                    <input class="form-control" type="text" name="status">
                </div>
                <div class="col-md-4">
                    <label for="username">Gender</label>
                    <input class="form-control" type="text" name="gender">
                </div>
            </div>





        </form>

    </div>
</div>

@stop