@extends('auth')

@section('content')

    <form action="login" method="post" accept-charset="utf-8">
        {!! csrf_field() !!}

        <div style="text-align:center; margin-bottom: 20px;">
            <img src="/img/logo.png" alt="">
        </div>
            <article class="panel panel-default auth-content">
                
                @include('errors.validate')
                <div class="panel-body">
                    <div class="col-md-12">
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                <input type="text" name="username" value="{{ old('username') }}" class="form-control" placeholder="ឈ្មោះគណនី">
                            </div>
                            <br>

                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                <input type="password" name="password" value="" class="form-control" placeholder="ពាក្យសម្ងាត់">
                            </div>
                            <br>

                            <div class="checkbox hide">
                                <label><input type="checkbox" name="remember" value="1">
                                    Remember me</label>
                            </div>
                    </div>

                </div>

                <div class="panel-footer clearfix">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" name="auth_submit" value="ចូល" class="btn btn-block btn-info">
                            </div>
                        </div>
                    </div>
                </div>

            </article>
    </form>

@stop
