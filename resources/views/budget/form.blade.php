@if (isset($hidden_fields))
    @foreach ($hidden_fields as $key => $val)
        {!! Form::hidden($key, $val) !!}
    @endforeach
@endif

@foreach ($chapters as $chapter)
    <div class="form-group">
        <label for="{{ $chapter->en_no }}">ជំពូកទី{{ $chapter->kh_no }}</label>
        {!! Form::text('chapter_'.$chapter->en_no, null, array(
                'id' => $chapter->en_no,
                'class'=>'form-control number-only',
                'maxlength'=>'14',
                'placeholder' => 'បញ្ជូលទិនន្នន័យ ជំពូកទី' . $chapter->kh_no,
                'title' => ''
        )) !!}
    </div>
@endforeach

<button type="submit" class="btn btn-default">{!! $submittype !!}</button>
<a href="/" class="btn btn-danger">ត្រឡប់</a>

@push('scripts')
    {!! HTML::script('js/jquery.number.min.js') !!}
    {!! HTML::script('js/dash.js') !!}
@endpush