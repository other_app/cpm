<!-- Modal -->
<div class="modal fade" id="{{ isset($id) ? $id : 'myModal' }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    {{ $title }}
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                @if (isset($include))
                    @include($include)
                @elseif (isset($content))
                    {{ $content }}
                @endif
            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    ចាកចេញ
                </button>
                <button type="button" class="btn btn-primary">
                    រក្សាទុក
                </button>
            </div>

        </div>
    </div>
</div>
