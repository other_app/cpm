@extends('master')

@section('content')
    <?php $user_role = Auth::user()->role == 'user';?>
    @include('partials.flash')

    <div id="divBudget">
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-header">ផែនការសាច់ប្រាក់របស់  {{ $user_role ? Auth::user()->ministry->name : 'ក្រសួងនីមួយៗ' }} </h3>
            </div>
        </div>
        @if ($user_role)
            <div class="row">
                <div class="col-md-offset-4 col-md-4">
                    {!! link_to_route('dash.budget.create_income', 'បញ្ចូលប្រាក់ចំណូលថ្មី', null, ['class' => 'btn btn-default']) !!}
                    {!! link_to_route('dash.budget.create_expense', 'បញ្ចូលប្រាក់ចំណាយថ្មី', null, ['class' => 'btn btn-default']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <hr/>
                </div>
            </div>
        @endif

        <div class="row">
            {!! Form::open(['route' => $route_name, 'class' => 'form-inline', 'id' => 'frmBudget']) !!}
                <div class="col-md-9">
                    @if (!$user_role)
                        <div class="form-group">
                            <select name="ministry" id="selMinistry" class="form-control">
                                <option value="">
                                    <span>គ្រប់ក្រសួងទាំងឣស់​</span>
                                </option>
                                @foreach ($ministries as $i => $ministry)
                                    <option value="{{$i}}"><span>{{$ministry}}</span></option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                    <div class="form-group">
                        <select name="budget_type" id="selBudgetType" class="form-control">
                            <option value="income"><span>ចំណូល</span></option>
                            <option value="expense"><span>ចំណាយ</span></option>
                        </select>
                    </div>

                    <div class="form-group">
                        <select name="select_date" id="selDate" class="form-control">
                            <option value="daily">ប្រចាំថ្ងៃ</option>
                            <option value="weekly">ប្រចាំសប្តាហ៍</option>
                            <option value="monthly">ប្រចាំខែ</option>
                            <option value="yearly">ប្រចាំឆ្នាំ</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <div class='input-group date' id='datetime_picker'>
                            <input type='text' name="date" class="form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>

            <div class="col-md-3">
                <div class="pull-right">
                    <label>
                        <input type="checkbox" id="cbxIsTotal" name="is_total" value="true">
                        <span>បង្ហាញទិន្ន័យសរុប</span>
                    </label>
                    @if (!$user_role)
                    <label>
                        <input type="checkbox" id="cbxIsAdmin" name="is_admin" value="true">
                        <span>បង្ហាញទិន្ន័យអ្នកត្រួតពិនិត្យ</span>
                    </label>
                    @endif
                    <input class="btn btn-primary" type="submit" name="frmBudgetSubmit" value="ស្វែងរក">
                </div>
            </div>
        </div>
        {!! Form::close() !!}

        <div class="row">
            <div class="col-md-12">
                <hr/>
            </div>
        </div>

        <div class="data-inform">
            <div class="alert alert-info">
                <strong>ជូនដំណឹង!</strong> លក្ខណ័​​ដែលបានជ្រើសរើសនេះមិនត្រឹមត្រូវ ឬ គ្មានទិន្នន័យសំរាប់បង្ហាញ សូមធ្វើការជ្រើសរើសលក្ខ័ណដើប្បីបង្ហាញទិន្នន័យ។
            </div>
        </div>

        <div class="row data-containter">
            <div class="col-md-12">
                <div class="page-print responsive1" id="tblBudget​​">
                    {{-- Using JavaScript generate table --}}
                </div>
            </div>
        </div>
    </div>

    <div id="divReport">
        {{-- Using Jquery load file from /dash/report_income or /dash/report_expense --}}
    </div>

@stop

@push('scripts')
{!! HTML::script('js/jquery.number.min.js') !!}
{!! HTML::script('js/budget.js') !!}
@endpush