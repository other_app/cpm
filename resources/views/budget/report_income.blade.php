<div>
    <h2 class="page-header" id="lblMinistry">
        ផែនការចំណូលរបស់ក្រសួង អប់រំយុវជន និងកីឡា តាមជំពូក
    </h2>
</div>
<table id="btnIncomePrint" class="table table-bordered">
    <thead>
        <tr>
            <th>
                ជំពូក
            </th>
            <th>
                បរិយាយ
            </th>
            <th>
                ច្បាប់ថវិកា
            </th>
            <th>
                ឥណទានថ្មី
            </th>
            <th>
                សរុបរួម
            </th>
            <th>
                ចំណូល(%)
            </th>
            <th id="lblDate">10-02-2016</th>
            <th>
                សរុបយោង
            </th>
            <th>
                ឥទានថ្មី&#8203;&#8203; %
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
            <td class="title">សរុបរួមចំណូលថវិការរបស់រដ្ឋ (ក+ខ)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ក.សរុបចំណូលចរន្ត (ក្រុមទី១+ក្រុមទី២)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ក្រុមទី១៖ ចំណូលពិត (ប្រភេទទី១+ប្រភេទទី២)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទទី១៖ ចំណូលសារពើពន្ធ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title-bold">ចំណូលពន្ធគយ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title-bold">ចំណូលពន្ធដារ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title-bold">ចំណូលសារពើពន្ធផ្សេងៗ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title-italic">ក្នុងនោះចំណូលសារពើពន្ធតាមជំពូក៖</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៧០</td>
            <td>ចំណូលសារពើពន្ធក្នុងស្រុក</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_70" class="number-only">11.00</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៧១</td>
            <td>ចំណូលពន្ធលើពាណិជ្ជកម្មក្រៅប្រទេស</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_71" class="number-only">122.00</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទទី២៖ ចំណូលមិនមែនសារពើពន្ធ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៧២</td>
            <td>ផលទុននៃទ្រព្យសម្បត្តិរដ្ឋ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_72" class="number-only">3.00</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៧៣</td>
            <td>ផលទុនពីការលក់ជួលទ្រព្យសម្បត្តិ និងសេវា</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_73" class="number-only">33.00</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៧៤</td>
            <td>ចំណូលពីការផាកពិន័យ និងការដាក់ទណ្ឌកម្ម</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_74" class="number-only">44.00</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៧៥</td>
            <td>ការឧបត្ថម្ភអំណោយ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_75" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៧៦</td>
            <td>ផលហិរញ្ញវត្ថុ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_76" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៧៧</td>
            <td>ផលផ្សេងៗ និងផលពិសេស</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_77" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ក្រុមទី២៖ ចំណូលតាមដីកា</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទទី៣៖ ចំណូលតាមដីកា (មិនឆ្លងកាត់សាច់ប្រាក់)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៧៨</td>
            <td>ការបញ្ចូលវិញនូវសំវិធានធន និងរំលស់អចលកម្ម</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_78" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ខ.សរុបមូលធន(ក្រុមទិ១+ក្រុមទី២)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ក្រុមទី១៖ ចំណូលពិត (ប្រភេទទី១+ប្រភេទទី២)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទទី១៖ ចំណូលពីប្រភពផ្ទាល់</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>២៧</td>
            <td>អចលកម្មហិរញ្ញវត្ថុផ្សេងៗ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_27" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទទី២៖ ចំណូលពីប្រភពខាងក្រៅ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៥០</td>
            <td>ការខ្ចីប្រាក់ និងបំណុលប្រហាក់ប្រហែល</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_50" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ក្រុមទី២៖ ចំណូលតាមដីកា</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ក្រុមទី៣៖ ចំណូលតាមដីកា(មិនឆ្លងកាត់សាច់ប្រាក់)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
<div class="no-print">
    <br>
    <button class="btn btn-info" id="btnPrint">បោះពុម្ភ</button>
    <button class="btn btn-default" id="btnHideReport">ត្រលប់ទៅទំព័រមុន</button>
</div>