@extends('master')

@section('content')

<div style="width:800px;" class="container">
    <div class="row" style="margin-top:10px;">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="text-center">{!! $title !!}</h3></div>
                <div class="panel-body">
                    {!! Form::model($budget, ['method' => 'PATCH', 'route' => [$route_name, $budget->id]])!!}
                    {{--@include('budget.form', ['chapters' => $chapters, 'ministries' => $ministries, 'submittype' => 'រក្សាទុក'])--}}
                    @include('budget.form', ['chapters' => $chapters, 'submittype' => 'រក្សាទុក'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <hr>
        </div>
    </div>
    
</div>
@stop
