<div>
    <h2 class="page-header" id="lblMinistry">
        ផែនការចំណាយរបស់ក្រសួង អប់រំយុវជន និងកីឡា តាមជំពូក
    </h2>
</div>
<table id="btnExpensePrint" class="table table-bordered">
    <thead>
        <tr>
            <th>
                ជំពូក
            </th>
            <th>
                បរិយាយ
            </th>
            <th>
                ច្បាប់ថវិកា
            </th>
            <th>
                ឥណទានថ្មី
            </th>
            <th>
                សរុបរួម
            </th>
            <th>
                ចំណូល(%)
            </th>
            <th id="lblDate">
                21-02-2016
            </th>
            <th>
                សរុបយោង
            </th>
            <th>
                ឥទានថ្មី&#8203;&#8203; %
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
            <td class="title">សរុបរួមចំណាយថវិការរបស់រដ្ឋ (ក+ខ)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ក.សរុបចំណាយចរន្ត (ក្រុមទី១+ក្រុមទី២)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ក្រុមទី១៖ ចំណាយពិត</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទទី១៖ មធ្យោបាយសេវា</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៦០</td>
            <td>ការទិញ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_60" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៦១</td>
            <td>សេវាកម្ម</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_61" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៦៤</td>
            <td>បន្ទុកបុគ្គលិក</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_64" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទទី២៖ បន្ទុកហិរញ្ញវត្ថុ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៦៦</td>
            <td>បន្ទុកហិរញ្ញវត</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_66" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទទី៣៖ អន្តរាគមន៏សារធារណៈ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៦២</td>
            <td>អត្ថប្រយោជន៏សង្គម</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_62" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៦៥</td>
            <td>ឧបត្ថម្ភកធន</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_65" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទទី៤៖ ចំណាយផ្សេងៗ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៦៣</td>
            <td>ពន្ធអាករ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_63" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ក្រុមទី២៖ ចំណាយតាមដីក និង ចំណាយមិនបានគ្រោងទុក&#8203; (ប្រភេទទី៥+ប្រភេទទី៦)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទទី៥៖ ចំណាយតាមដីកា (មិនឆ្លងសាច់ប្រាក់)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៦៧</td>
            <td>បន្ទុកពិសេស</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_67" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៦៨</td>
            <td>ទាយជ្ជទានឲ្យដល់រំលស់អចលកម្ម</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_68" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទទី៦៖ ចំណាយមិនបានគ្រោងទុក</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៦៩</td>
            <td>ចំណាយមិនបានគ្រោងទុក</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_69" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ខ. សរុបចំណាយជាមូលធន (ក្រុមទី១+ក្រុមទី២)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ក្រុមទី១៖ ចំណាយពិត (ប្រភេទទី១+ប្រភេទទី២)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទទី១៖ ការទូទាត់ប្រាក់ខ្ជី និងបំណុលប្រហាក់ប្រហែល</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>៥០</td>
            <td>ការខ្ជី</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_50" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទទី២៖ អចលកម្ម</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>២០</td>
            <td>អចលកម្មអរូបី</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_20" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>២១</td>
            <td>អចលកម្មរូបី</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_21" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>២២</td>
            <td>ទ្រព្យសម្បត្តិវិនិយោគរយះពេលវែង</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_22" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>២៣</td>
            <td>អចលកម្មកំពុងដំណើរការ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_23" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>២៦</td>
            <td>ការវិនិយោគ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_26" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>២៧</td>
            <td>អចលកម្មហិរញ្ញវត្ថុផ្សេងៗ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td id="chapter_27" class="number-only"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ក្រុមទី២៖ ចំណាយតាមដីកា</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="title">ប្រភេទី៣៖ ចំណាយតាមដីកា</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>

<div class="no-print">
    <br>
    <button class="btn btn-info" id="btnPrint">បោះពុម្ភ</button>
    <button class="btn btn-default" id="btnHideReport">ត្រលប់ទៅទំព័រមុន</button>
</div>