@extends('master')

@section('content')

<div style="width:800px;" class="container">
@include('partials.flash')


    <div class="row" style="margin-top:10px;">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="text-center">{!! $title !!}</h3></div>
                <div class="panel-body">
                    {!! Form::open(['route' => $route_name, 'id' => 'frmBudget', 'role' => 'form form-horizontal']) !!}
                    
                        {{--@include('budget.form', ['chapters'=>$chapters, 'ministries' => $ministries]) --}}
                        
                        <div class="form-group">
                            <label for="selMinistry">កាលបរិច្ឆេទ</label>
                            <div class="input-group date" id="datetime_picker">
                                <input type="text" name="created_at" id="created_date" class="form-control" required>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        
                        <input type="hidden" name="ministry_id" value="{{ Auth::user()->ministry->id }}">

                        @include('budget.form', ['chapters'=>$chapters])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <hr>
        </div>
    </div>

</div>
@stop
