@extends('master')

@section('title')
    ក្រសួង នីមួយៗ
@endsection


@section('content')

  <h2 class="page-header">បញ្ជីឈ្មោះក្រសួងទាំងឣស់
        <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#modalMinistry">
          បន្ថែមថ្មី
        </button>
  </h2>

    <div class="responsive">
        <table id="tblMinistry" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>ឈ្មោះក្រសួង</th>
                    <th>ថ្ងៃបង្កើត</th>
                    <th>ថ្ងៃកែប្រែ</th>
                    <th>ស្ថានភាព</th>
                    <th>បង្កើតដោយ</th>
                    <th>ធ្វើការកែប្រែ</th>
                </tr>
            </thead>
            <tbody>
            @foreach($ministries as $ministry)
                <tr>
                    <td>{{ $ministry->name }}</td>
                    <td>{{ date('d-m-Y', strtotime($ministry->created_at)) }}</td>
                    <td>{{ date('d-m-Y', strtotime($ministry->updated_at)) }}</td>
                    <td>{{ $ministry->status == '1' ? 'on' : 'off' }}</td>
                    <td>{{ $ministry->created_by }}</td>
                    <td>
                    {!! Form::open(['data-remote', 'method' => 'GET', 'url' => 'dash/ministry/' . $ministry->id, 'id' => 'ministryIdEditBtn' ]) !!}
                        <button type="submit" class="btn btn-default btn-sm">
                          <span class="glyphicon glyphicon-edit"></span>
                      </button>
                  {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


<!-- Ministry Form -->
<div class="modal fade" id="modalMinistry" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ក្រសួងថ្មី</h4>
      </div>

      {!! Form::open(['data-remote', 'id' => 'formMinistry']) !!}

      <div id="formValidation">
        <div class="alert alert-success"></div>
        <div class="alert alert-danger"></div>
      </div>

      <div class="modal-body">

        <div class="form-group">
        {!! Form::label('name', 'ឈ្មោះក្រសួង:', ['class' => 'control-label']) !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label for="ministry_status" class="control-label">ស្ថានភាព:</label>
            <div class="radio">
                <label>{!! Form::radio('status', true, 'on') !!} ON</label>
            </div>
            <div class="radio">
                <label>{!! Form::radio('status', 'off') !!} OFF</label>
            </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">បិទ</button>
        {!! Form::submit('បញ្ចូល', ['class' => 'btn btn-primary', 'id' => 'btnMinistry']) !!}
      </div>
        {!! Form::close() !!}
    </div>
  </div>
</div>

@endsection

@push('scripts')
    {!! HTML::script('js/ministry.js') !!}
@endpush
