@extends('master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">បញ្ជីឈ្មោះអ្នកប្រើ
                {!! link_to_route('dash.user.create', 'បន្ថែមថ្មី', null, ['class' => 'btn btn-default pull-right']) !!}
            </h2>
        </div>
    </div>
    <table class="table table-bordered table-hover myStyle" id="tblUsers">
        <thead>
            <tr>
                <th>ឈ្មោះគណនី</th>
                <th>ឈ្មោះ</th>
                <th>នាមត្រកូល</th>
                <th>អ៊ីម៉ែល</th>
                <th>ភេទ</th>
                <th>ថ្ងៃខែ​ឆ្នាំ​កំណើត</th>
                <th>ធ្វើការកែប្រែ</th>
            </tr>
        </thead>
    </table>
    <hr/><br/>
@stop

@push('scripts')
    <script type="text/javascript">
        var _link = '{{ url() }}/';
        var _code = '{{ csrf_token() }}';
    </script>

    {!! HTML::style('css/datatables.css') !!}
    {!! HTML::script('js/datatables.js') !!}
    {!! HTML::script('js/user.js') !!}
@endpush
