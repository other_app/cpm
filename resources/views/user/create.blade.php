@extends('master')


@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">បង្កើតគណនី</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-8">
                    @include('errors.validate')
                    {!! Form::open(['route' => 'dash.user.store']) !!}
                        @include('user.form',  ['submittype' => 'Add'])
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

    <hr/><br/>
@stop
