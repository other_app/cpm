@if (Auth::user()->role == "admin")
    <div class="row">

        <div class="form-group col-md-6">
            {!! Form::label('ministry_id','ក្រសួង') !!}
            {!! Form::select('ministry_id', $ministries, $ministry, ['placeholder' => 'សូមជ្រើសរើស ...', 'class' => 'form-control']) !!}
        </div>

        <div class="form-group col-md-6">
            {!! Form::label('role','តួនាទី') !!}
            {!! Form::select('role', ['user' => 'User', 'admin' => 'Admin', 'manager' => 'Manager'], null, ['placeholder' => 'សូមជ្រើសរើស ...', 'class' => 'form-control']) !!}
        </div>
    </div>
@endif

{{-- */$disabled = !empty($user) ? 'disabled' : '';/* --}}

<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('firstname','ឈ្មោះ') !!}
        {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('lastname','នាមត្រកូល') !!}
        {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('username','ឈ្មោះគណនី') !!}
        {!! Form::text('username', null, ['class' => 'form-control', $disabled]) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('date_of_birth','ថ្ងៃខែ​ឆ្នាំ​កំណើត') !!}
        {!! Form::text('date_of_birth', null, ['class' => 'form-control', 'id' => 'txtDOB']) !!}
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('password','ពាក្យសម្ងាត់') !!}
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '*********']) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('ផ្ទៀងផ្ទាត់ ពាក្យសម្ងាត់','ផ្ទៀងផ្ទាត់ ពាក្យសម្ងាត់') !!}
        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => '*********']) !!}
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('email','អ៊ីម៉ែល') !!}
        {!! Form::email('email', null, ['class' => 'form-control', $disabled]) !!}
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('gender','ភេទ') !!}
        {!! Form::select('gender', ['' => 'សូមជ្រើសរើស ...', 'male' => 'Male', 'female' => 'Female'], null, ['placeholder' => 'សូមជ្រើសរើស ...', 'class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('status','ឯកសិទ្ធិ') !!}
        {!! Form::select('status', ['1' => 'on', '0' => 'off'], null, ['placeholder' => 'សូមជ្រើសរើស ...', 'class' => 'form-control']) !!}
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-4 pull-right">
        <div class="form-group">
            {!! link_to_route('dash.budget.index', 'ចាកចេញ', null, ['class' => 'btn btn-block btn-default']) !!}
        </div>
    </div>
    <div class="col-md-4 pull-right">
        <div class="form-group">
            {!! Form::submit('រក្សាទុក', ['class' => 'btn btn-block btn-info']) !!}
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
$(function() {
    $('#txtDOB').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
        language: 'kh',
    });

    // var $('.password').attr({
    //     type: 'password',
    //     placeholder: '*********'
    // });

    // var old = $('.password').first().val();
    // $('.password').attr('type', 'password').val(old);
});
</script>
@endpush
