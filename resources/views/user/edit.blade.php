@extends('master')


@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">កែសម្រួលគណនី របស់អ្នកប្រើប្រាស់ {!! ucfirst($user->username) !!}</h1>
        </div>
    </div>
   
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-8">
                    @include('errors.validate')
                    {!! Form::model($user, ['method' => 'PATCH', 'action' => ['UserController@update', $user->id]])!!}
                        @include('user.form', ['submittype' => 'Update'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
            <hr/><br/>
@stop
