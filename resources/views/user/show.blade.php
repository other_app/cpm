@extends('master') @section('content')
<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">ពត៌មានលមិ្ឣតរបស់៖​​ {{$user->username}}</h2>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-8">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="id">ក្រសួង </label>
                        <p class="form-control">{{ $user->ministry->name }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="id">តួនាទី </label>
                        <p class="form-control">{{ $user->role }}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="id">ឈ្មោះ </label>
                        <p class="form-control">{{ $user->firstname }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="firstname">នាមត្រកូល </label>
                        <p class="form-control">{{ $user->lastname }}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="id" class="text-danger">ឈ្មោះគណនី </label>
                        <p class="form-control">{{ $user->username }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="id">ថ្ងៃខែ​ឆ្នាំ​កំណើត </label>
                        <p class="form-control">{{ $user->date_of_birth }}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="firstname" class="text-danger">អ៊ីម៉ែល </label>
                        <p class="form-control">{{ $user->email }}</p>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="id">ភេទ</label>
                        <p class="form-control">{{ $user->gender }}</p>
                    </div>

                    <div class="form-group col-md-3">
                        {!! Form::label('status','ឯកសិទ្ធិ') !!}
                        {!! Form::select('status', ['1' => 'on', '0' => 'off'], $user->status , ['class' => 'form-control disabled', 'disabled']) !!}
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-md-4 pull-right">
                        <div class="form-group">
                            {!! link_to_route('dash.user.index', 'ចាកចេញ', null, ['class' => 'btn btn-block btn-default']) !!}
                        </div>
                    </div>
                    <div class="col-md-4 pull-right">
                        <div class="form-group">
                            {!! link_to_route('dash.user.edit', 'ធ្វើការកែសម្រួល', $user->id, ['class' => 'btn btn-block btn-info']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr/>
<br/> @stop
