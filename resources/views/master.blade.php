<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ url('favicon.ico') }}">
    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    {!! HTML::style('fonts/khmer/khmer-webfonts.css') !!}
    {!! HTML::style('css/khmer-font.css') !!}
    {!! HTML::style('css/bootstrap.min.css', ["media" => "screen"]) !!}

    <!-- Custom styles for this template -->
    {!! HTML::style('css/bootstrap-custome-theme.css', ["media" => "screen"]) !!}

    {{-- Custom style for Virak --}}
    {!! HTML::style('css/virak/style.css', ["media" => "screen"]) !!}

    {{-- Print style by Ravuth --}}
    {!! HTML::style('css/print.css', ["media" => "print"]) !!}

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="{{url('js/ie-emulation-modes-warning.js')}}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-link="{!! url() !!}">

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top no-print">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="{{url()}}">
                    <img src="{{url('img/logo.png')}}" height="50px" width="auto">
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                        @if (Auth::user()->role == 'admin')
                        <li class="{{ Request::is('dash.user') ? 'active' : '' }}">
                            {!! link_to_route('dash.user.index', 'គ្រប់គ្រងគណនីរបស់អ្នកប្រើប្រាស់') !!}
                        </li>
                        <li class="{{ Request::is('dash.ministry') ? 'active' : '' }}">
                            {!! link_to_route('dash.ministry.index', 'គ្រប់គ្រងក្រសួង') !!}
                        </li>
                    @endif
                    {{-- <li class="{{ Request::is('dash.report') ? 'active' : '' }}">
                        {!! link_to_route('dash.report', 'របាយការណ៏') !!}
                    </li> --}}
                    {{-- <li class="{{ Request::is('dash.report') ? 'active' : '' }}">
                        {!! Html::link('#report', 'បង្ហាញរបាយការណ៏', ['id' => 'btnShowReport']) !!}
                    </li>
                    <li class="{{ Request::is('dash.report') ? 'active' : '' }}">
                        <a href="javascript:" id="btnShowReport">បង្ហាញរបាយការណ៏</a>
                    </li>
                    <li class="{{ Request::is('dash.report') ? 'active' : '' }}">
                        <a href="javascript:" id="btnHideReport">បង្ហាញរបាយការណ៏</a>
                    </li> --}}
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li>
                        {!! link_to_route('dash.user.edit', ucfirst(Auth::user()->username), Auth::user()->id) !!}
                    </li>
                    <li>{!! link_to_route('auth.logout', 'ចាកចេញ') !!}</li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <div class="container" style="padding: 10px 15px 0 !important">
        @yield('content')
    </div>
    <br/>
    <br/>
    <br/>
    <footer class="footer no-print">
        <div class="container">
            <center class="text-muted">
                <p>អាគារលេខ ១៩ រុក្ខវិថីព្រះមហាក្សត្រីយានីកុសមៈ (ផ្លូវលេខ ១០៦) សង្កាត់វត្តភ្នំ ខណ្ឌដូនពេញ រាជធានីភ្នំពេញ ព្រះរាជាណាចក្រកម្ពុជាអគ្គនាយកដ្ឋានរតនាគារជាតិ</p>
                <p class="en">General Department of National Treasury</p>
            </center>
        </div>
    </footer>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="{{url('js/jquery-1.11.3.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{url('js/ie10-viewport-bug-workaround.js')}}"></script>
    {!! HTML::style('css/bootstrap-datetimepicker.min.css') !!} {{-- {!! HTML::script('js/jquery-1.11.3.min.js') !!} --}} {!! HTML::script('js/moment.min.js') !!} {{-- {!! HTML::script('js/bootstrap.min.js') !!} --}} {!! HTML::script('js/bootstrap-datetimepicker.min.js') !!}
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    {!! HTML::style('css/dataTables.bootstrap.min.css') !!}
    {!! HTML::script('js/jquery.dataTables.min.js') !!}
    {!! HTML::script('js/dataTables.bootstrap.min.js') !!}
    {!! HTML::script('js/dash.config.js') !!}
    @stack('scripts')
    <!-- Custom script Virak -->
    <script src="{{url('js/customs/ministry.js')}}"></script>
</body>

</html>
