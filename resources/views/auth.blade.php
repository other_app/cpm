<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ url('favicon.ico') }}">
    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="{{url('fonts/khmer/khmer-webfonts.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.min.css')}}">
    <!-- Custom styles for this template -->
    <link href="{{url('css/navbar-fixed-top.css')}}" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="{{url('js/ie-emulation-modes-warning.js')}}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        body {
            background-color: #edf8f6;
        }
        * {
            border-radius: 1px !important;
        }
        .btn-block {
            margin-top:15px;
            margin-bottom: 15px;
        }

        h3 {
            text-align: center;
        }

        .auth-container {
            margin-top: 70px;
        }
        /*.auth-content {
            -webkit-box-shadow: 10px 10px 18px 0px rgba(39,59,79,1);
            -moz-box-shadow: 10px 10px 18px 0px rgba(39,59,79,1);
            box-shadow: 10px 10px 18px 0px rgba(39,59,79,1);
        }*/
        h1, h2, h3, h4 { font-family: 'KhmerOSmuol' }
        p, input { font-family: 'KhmerOSbattambang' }
    </style>
</head>

<body>

    <div class="container" style="width: 450px; margin-top: 50px;">


        @yield('content')

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="{{url('js/jquery-1.11.3.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{url('js/ie10-viewport-bug-workaround.js')}}"></script>
    @stack('scripts')
</body>

</html>
