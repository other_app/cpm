<style>
    .title {
        font-size: 50px;
        margin-bottom: 20px;
    }
</style>
@extends('master')

@section('content')

    <div class="container">
            <div class="text-center">
                <div class="title">{{ $title }}</div>
                <p>{{ $description }}</p>
                <br/>
                {!! link_to_route('dash.budget.index', 'ត្រឡប់ទៅទំព័រដើម', null, ['class' => 'btn btn-default btn-lg']) !!}
            </div>
        </div>
@stop
