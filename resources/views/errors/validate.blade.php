@if($errors->any())
	<div class="alert alert-danger">
		<strong>សូមអភ័យទោស​!</strong> សូមពិនិត្យមើលប្រអប់អត្ថបទថ្មីម្តងទៀត!!!<br><br>
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif
