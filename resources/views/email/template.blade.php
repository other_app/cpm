<h1>User Created</h1>

<p>Username: {{ $user['username'] }}</p>
<p>Password: {{ $password }}</p>
<p>Ministry id: {{ $user['ministry_id'] }}</p>
<p>User role: {{ $user['role'] }}</p>
<p>Created at: {{ $user['created_at'] }}</p>
<p>Updated at: {{ $user['updated_at'] }}</p>