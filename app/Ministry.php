<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ministry extends Model {
    public function adminIncome() {
        return $this->belongsTo('App\AdminIncome');
    }

    protected $fillable = [
        'name', 'status'
    ];

    /**
     * A ministry is created by a user.
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

}
