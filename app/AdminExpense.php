<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminExpense extends Model {
    protected $fillable = [
        'user_id',
        'ministry_id',
        'chapter_60',
        'chapter_61',
        'chapter_62',
        'chapter_63',
        'chapter_64',
        'chapter_65',
        'chapter_66',
        'chapter_67',
        'chapter_68',
        'chapter_69',
        'chapter_20',
        'chapter_21',
        'chapter_22',
        'chapter_23',
        'chapter_26',
        'chapter_27',
        'chapter_50',
        'approved_id'
    ];
}
