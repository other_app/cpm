<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;

class User extends Model implements AuthenticatableContract,
AuthorizableContract,
CanResetPasswordContract {
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'users';
    
    protected $dates = ['date_of_birth'];

    protected $fillable = ['firstname', 'lastname', 'username', 'date_of_birth', 'password', 'email', 'role', 'gender', 'ministry_id', 'status'];

    protected $hidden = ['remember_token'];

    public function adminIncomes() {
        return $this->hasMany('App\AdminIncome', 'user_id', 'id');
    }

    public function adminExpense() {
        return $this->hasMany('App\AdminExpense', 'user_id', 'id');
    }

    public function incomes() {
        return $this->hasMany('App\UserIncome', 'user_id', 'id');
    }

    public function expense() {
        return $this->hasMany('App\UserExpense', 'user_id', 'id');
    }

    public function ministries() {
        return $this->hasMany('App\Ministry', 'created_by');
    }

    public function ministry() {
        return $this->hasOne('App\Ministry', 'id', 'ministry_id');
    }

    public function getDateOfBirthAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    }

}
