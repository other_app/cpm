<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RoleAsAdmin {
    protected $auth;

    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next) {
        $auth = $this->auth->user();

        if ($this->auth->user()->role == 'admin') {
            return $next($request);
        }

        $data['title'] = 'គ្មានការអនុញ្ញាត';
        $data['description'] = 'អ្នកត្រូវតែមានសិទ្ធជា Administator';
        return view('errors.401', $data);
    }
}
