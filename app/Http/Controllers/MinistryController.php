<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Requests\PrepareMinistryRequest;
use App\Ministry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MinistryController extends Controller {
    public function __construct() {
        parent::__construct();
        if ($this->user->role == 'user') {
            $this->middleware('admin', ['except' => ['edit', 'update']]);
        }
    }

    public function index() {
        //$ministries = Auth::user()->ministries;
        //return $ministries;
        $ministries = DB::table('ministries')
            ->join('users', 'users.id', '=', "ministries.created_by")
            ->get(['ministries.*', 'users.username AS created_by']);
        // $ministries = Ministry::all();

        
        return view('ministries.index', compact('ministries'));
    }

    public function store(PrepareMinistryRequest $request) {
        $ministry = $this->createMinistry($request);

        return $ministry;

    }

    public function show($id) {
        $ministry = Ministry::findOrFail($id);

        return $ministry;
    }

    public function update(PrepareMinistryRequest $request, $id) {
        $ministry = $this->editMinistry($request, $id);

        return $ministry;

    }

    // Create a new ministry entry.
    private function createMinistry(Request $request) {
        $ministry = $request->except('_token');

        Auth::user()->ministries()->create($ministry);

        return 'Add New Success';

    }

    // Edit Existing ministry.
    private function editMinistry(Request $request, $id) {
        $ministry = $request->except('_token', '_method');

        // Auth::user()->ministries()
        //             ->findOrFail($id)
        //             ->update($ministry);
        Ministry::findOrFail($id)->update($ministry);

        return 'Updated Success';

    }
}
