<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Chapter;
use App\Ministry;
use Carbon\Carbon;
use App\UserIncome;
use App\AdminIncome;
use App\UserExpense;
use App\AdminExpense;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BudgetController extends Controller {
    protected function listBudgets(Request $request) {
        $budget_type = $request->input('budget_type');
        $select_date = $request->input('select_date');
        $date = $request->input('date');

        if ($request->input('is_admin')) {
            $table = $budget_type == 'income' ? 'admin_incomes' : 'admin_expenses';
        } else {
            $table = $budget_type == 'income' ? 'user_incomes' : 'user_expenses';
        }

        $db = DB::table($table);
        $db->join('users', 'users.id', '=', "$table.user_id")
           ->join('ministries', 'ministries.id', '=', "$table.ministry_id")
           ->orderBy('ministries.id', 'asc')
           ->select(
               "$table.id AS id",
               "$table.approved_id AS approved_id",
               "ministries.id AS ministry_id",
               "ministries.name AS ministry_nm",
               "users.id AS user_id"
           );

        if ($this->user->role == 'user') {
            $db->groupBy("ministries.id");
            if ($budget_type == 'income') {
                $db->selectRaw("
                    SUM($table.`chapter_70`) as chapter_70,
                    SUM($table.`chapter_71`) as chapter_71,
                    SUM($table.`chapter_72`) as chapter_72,
                    SUM($table.`chapter_73`) as chapter_73,
                    SUM($table.`chapter_74`) as chapter_74,
                    SUM($table.`chapter_75`) as chapter_75,
                    SUM($table.`chapter_76`) as chapter_76,
                    SUM($table.`chapter_77`) as chapter_77,
                    SUM($table.`chapter_78`) as chapter_78,
                    SUM($table.`chapter_27`) as chapter_27,
                    SUM($table.`chapter_50`) as chapter_50
                ");
            } else {
                $db->selectRaw("
                   SUM($table.`chapter_60`) as chapter_60,
                   SUM($table.`chapter_61`) as chapter_61,
                   SUM($table.`chapter_62`) as chapter_62,
                   SUM($table.`chapter_63`) as chapter_63,
                   SUM($table.`chapter_64`) as chapter_64,
                   SUM($table.`chapter_65`) as chapter_65,
                   SUM($table.`chapter_66`) as chapter_66,
                   SUM($table.`chapter_67`) as chapter_67,
                   SUM($table.`chapter_68`) as chapter_68,
                   SUM($table.`chapter_69`) as chapter_69,
                   SUM($table.`chapter_20`) as chapter_20,
                   SUM($table.`chapter_21`) as chapter_21,
                   SUM($table.`chapter_22`) as chapter_22,
                   SUM($table.`chapter_23`) as chapter_23,
                   SUM($table.`chapter_26`) as chapter_26,
                   SUM($table.`chapter_27`) as chapter_27,
                   SUM($table.`chapter_50`) as chapter_50
                ");
            }
        } else {
            if ($request->input('is_total')) {
                $db->groupBy("ministries.id");
                if ($budget_type == 'income') {
                    $db->selectRaw("
                        SUM($table.`chapter_70`) as chapter_70,
                        SUM($table.`chapter_71`) as chapter_71,
                        SUM($table.`chapter_72`) as chapter_72,
                        SUM($table.`chapter_73`) as chapter_73,
                        SUM($table.`chapter_74`) as chapter_74,
                        SUM($table.`chapter_75`) as chapter_75,
                        SUM($table.`chapter_76`) as chapter_76,
                        SUM($table.`chapter_77`) as chapter_77,
                        SUM($table.`chapter_78`) as chapter_78,
                        SUM($table.`chapter_27`) as chapter_27,
                        SUM($table.`chapter_50`) as chapter_50
                    ");
                } else {
                    $db->selectRaw("
                       SUM($table.`chapter_60`) as chapter_60,
                       SUM($table.`chapter_61`) as chapter_61,
                       SUM($table.`chapter_62`) as chapter_62,
                       SUM($table.`chapter_63`) as chapter_63,
                       SUM($table.`chapter_64`) as chapter_64,
                       SUM($table.`chapter_65`) as chapter_65,
                       SUM($table.`chapter_66`) as chapter_66,
                       SUM($table.`chapter_67`) as chapter_67,
                       SUM($table.`chapter_68`) as chapter_68,
                       SUM($table.`chapter_69`) as chapter_69,
                       SUM($table.`chapter_20`) as chapter_20,
                       SUM($table.`chapter_21`) as chapter_21,
                       SUM($table.`chapter_22`) as chapter_22,
                       SUM($table.`chapter_23`) as chapter_23,
                       SUM($table.`chapter_26`) as chapter_26,
                       SUM($table.`chapter_27`) as chapter_27,
                       SUM($table.`chapter_50`) as chapter_50
                    ");
                }
            } else {
                if ($budget_type == 'income') {
                    $db->selectRaw("
                        $table.`chapter_70` AS `chapter_70`,
                        $table.`chapter_71` AS `chapter_71`,
                        $table.`chapter_72` AS `chapter_72`,
                        $table.`chapter_73` AS `chapter_73`,
                        $table.`chapter_74` AS `chapter_74`,
                        $table.`chapter_75` AS `chapter_75`,
                        $table.`chapter_76` AS `chapter_76`,
                        $table.`chapter_77` AS `chapter_77`,
                        $table.`chapter_78` AS `chapter_78`,
                        $table.`chapter_27` AS `chapter_27`,
                        $table.`chapter_50` AS `chapter_50`
                    ");
                } else {
                    $db->selectRaw("
                       $table.`chapter_60` AS `chapter_60`,
                       $table.`chapter_61` AS `chapter_61`,
                       $table.`chapter_62` AS `chapter_62`,
                       $table.`chapter_63` AS `chapter_63`,
                       $table.`chapter_64` AS `chapter_64`,
                       $table.`chapter_65` AS `chapter_65`,
                       $table.`chapter_66` AS `chapter_66`,
                       $table.`chapter_67` AS `chapter_67`,
                       $table.`chapter_68` AS `chapter_68`,
                       $table.`chapter_69` AS `chapter_69`,
                       $table.`chapter_20` AS `chapter_20`,
                       $table.`chapter_21` AS `chapter_21`,
                       $table.`chapter_22` AS `chapter_22`,
                       $table.`chapter_23` AS `chapter_23`,
                       $table.`chapter_26` AS `chapter_26`,
                       $table.`chapter_27` AS `chapter_27`,
                       $table.`chapter_50` AS `chapter_50`
                    ");
                }
            }
        }

        if (!empty($select_date) && !empty($date)) {
            if ($select_date == 'daily') {
                $db->whereDate("$table.created_at", '=', $date);
            } else if ($select_date == 'weekly') {
                $db->whereRaw("WEEKOFYEAR($table.created_at) = ?", [$date]);
            } else if ($select_date == 'monthly') {
                $db->whereMonth("$table.created_at", '=', $date);
            } else {
                $db->whereYear("$table.created_at", '=', $date);
            }
        }

        if ($this->user->role == 'admin') {
            if ($request->has('ministry')) {
                $ministry = $request->input('ministry');
                $db->where('ministries.id', $ministry);
            }
        } else {
            $db->where('ministries.id', $this->user->ministry_id);
            $db->where('users.id', $this->user->id);
        }

        return $db;
    }

    public function json(Request $request) {
        $data['budgets'] = $this->listBudgets($request)->get();

        $data['auth'] = [
            'id'        => Auth::user()->id,
            'role'      => Auth::user()->role,
            'email'     => Auth::user()->email,
            'username'  => Auth::user()->username,
            'lastname'  => Auth::user()->lastname,
            'firstname' => Auth::user()->firstname
        ];
        if ($request->input('budget_type') == 'income') {
            $data['incomes'] = Chapter::income();
        } else {
            $data['expenses'] = Chapter::expense();
        }
        return $data;
    }

    public function index(Request $req) {
        // return $this->listBudgets($req)->toSql();

        $data['route_name'] = 'dash.budget.json';
        $data['ministries'] = Ministry::lists('name', 'id');
        return view('budget.index', $data);
    }

    public function create_income() {
        $data['title'] = 'ទម្រង់បែបបទ នៃការបញ្ចូលចំណូល';
        $data['submittype'] = 'បញ្ចូលចំណូល';
        $data['route_name'] = 'dash.budget.store_income';
        $data['chapters'] = Chapter::income();
        // $data['ministries'] = Ministry::lists('name', 'id');
        return view('budget.create', $data);
    }

    public function create_expense() {
        $data['title'] = 'ទម្រង់បែបបទ នៃការបញ្ចូលចំណាយ';
        $data['submittype'] = 'បញ្ចូលចំណាយ';
        $data['route_name'] = 'dash.budget.store_expense';
        $data['chapters'] = Chapter::expense();
        $data['ministries'] = Ministry::lists('name', 'id');
        return view('budget.create', $data);
    }

    public function store_income(Request $request) {
        // Checking only one User and Day allow to insert. nl2br("\n")
        $inputDay = $this->compareDuplicatDate($request, 'income');
        if (!$inputDay) {
            return redirect()->back();
        }

        $income = $this->validNumbers($request->all());
        $income['created_at'] = $inputDay;
        if ($this->user->role == 'admin') {
            $this->user->adminIncomes()->create();
        } else {
            $this->user->incomes()->create($income);
        }
        $request->session()->flash('alert-success', 'ការបញ្ចូល ចំណូល ទទួលបានជោគជ័យ');
        return redirect('dash/budget');
    }

    public function store_expense(Request $request) {
        // Checking only one User and Day allow to insert. nl2br("\n")
        $inputDay = $this->compareDuplicatDate($request, 'expense');
        if (!$inputDay) {
            return redirect()->back();
        }

        $expense = $this->validNumbers($request->all());
        $expense['created_at'] = $inputDay;
        if ($this->user->role == 'admin') {
            $this->user->adminExpense()->create($request->except('_token'));
        } else {
            $this->user->expense()->create($expense);
        }

        $request->session()->flash('alert-success', 'ការបញ្ចូល ចំណាយ ទទួលបានជោគជ័យ');
        return redirect('dash/budget');
    }

    public function edit_income($id) {
        if ($this->user->role == 'admin') {
            $data['budget'] = AdminIncome::findOrFail($id);
        } else {
            $data['budget'] = UserIncome::findOrFail($id);
        }
        $data['title'] = 'កែប្រែប្រាក់ចំណូល';
        $data['route_name'] = 'dash.budget.update_income';
        $data['chapters'] = Chapter::income();
        $data['ministries'] = Ministry::lists('name', 'id');
        return view('budget.edit', $data);
    }

    public function edit_expense($id) {
        if ($this->user->role == 'admin') {
            $data['budget'] = AdminExpense::findOrFail($id);
        } else {
            $data['budget'] = UserExpense::findOrFail($id);
        }

        $data['title'] = 'កែប្រែប្រាក់ចំណាយ';
        $data['route_name'] = 'dash.budget.update_expense';
        $data['chapters'] = Chapter::expense();
        $data['ministries'] = Ministry::lists('name', 'id');
        return view('budget.edit', $data);
    }

    public function getApprove($type, $id) {
        if (Auth::user()->role == 'admin') {
            if ($type == 'income') {
                $budget = UserIncome::findOrFail($id);
                $data['chapters'] = Chapter::income();
            } else {
                $budget = UserExpense::findOrFail($id);
                $data['chapters'] = Chapter::expense();
            }

            $data['title'] = 'កែប្រែប្រាក់ចំណាយ ព្រមទាំងការអនុម័ត្ត';
            $data['route_name'] = 'dash.budget.patch_approved';
            $data['budget'] = $budget;
            $data['hidden_fields'] = [
                'budget_id'   => $id,
                'budget_type' => $type,
                'user_id'     => $budget->user_id
            ];
            return view('budget.edit', $data);
        }
    }

    public function patchApprove(Request $req) {
        if ($req->budget_type == 'income') {
            $budget = UserIncome::findOrFail($req->budget_id);
            $budget->update(['approved_id' => Auth::user()->id]);
            AdminIncome::create($budget->toArray());
        } else {
            $budget = UserExpense::findOrFail($req->budget_id);
            $budget->update(['approved_id' => Auth::user()->id]);
            AdminExpense::create($budget->toArray());
        }
        $req->session()->flash('alert-success', 'ការអនុម័ត្តទទួលបានជោគជ័យ');
        return redirect('dash/budget');
    }

    public function update_expense(Request $request, $id) {
        $data = $request->all();
        $data['user_id'] = $this->user->id;
        if ($this->user->role == 'admin') {
            $expense = AdminExpense::findOrFail($id);
            $expense->update($data);
        } else {
            $expense = UserExpense::findOrFail($id);
            $expense->update($data);
        }
        $request->session()->flash('alert-success', 'ការកែប្រែ ចំណាយ ទទួលបានជោគជ័យ');
        return redirect('dash/budget');
    }

    public function update_income(Request $request, $id) {
        $data = $request->all();
        $data['user_id'] = $this->user->id;
        if ($this->user->role == 'admin') {
            $income = AdminIncome::findOrFail($id);
            $income->update($data);
        } else {
            $income = UserIncome::findOrFail($id);
            $income->update($data);
        }
        $request->session()->flash('alert-success', 'ការកែប្រែ ចំណូល ទទួលបានជោគជ័យ');
        return redirect('dash/budget');
    }

    private function compareDuplicatDate(Request $request, $getDateTypes) {
        if ($getDateTypes == 'income') {
            $getDateTypes = UserIncome::where('user_id', '=', $this->signedIn->id)
                ->select('created_at')
                ->get();
        } else {
            $getDateTypes = UserExpense::where('user_id', '=', $this->signedIn->id)
                ->select('created_at')
                ->get();
        }

        $inputDay = Carbon::parse($request->input('created_at'));
        foreach ($getDateTypes as $getDateType) {
            $getDateType = Carbon::parse($getDateType->created_at);
            if ($inputDay->eq($getDateType)) {
                session()->flash('alert-danger', 'ទិន្ន័យនៅថ្ងៃនេះបានបញ្ចូលរួចហើយ សូមជ្រើសរើសថ្ងៃដ៏ទៃ!');
                return false;
            }
        }

        return $inputDay;
    }

    private function validNumbers($fields) {
        $vals = [];
        foreach ($fields as $key => $val) {
            $vals[$key] = str_replace(',', '', $val);
        }
        return $vals;
    }
}
