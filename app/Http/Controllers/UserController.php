<?php

namespace App\Http\Controllers;

use App\User;
use App\Ministry;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;

class UserController extends Controller {
    public function __construct() {
        parent::__construct();
        if ($this->user->role == 'user') {
            $this->middleware('admin', ['except' => ['edit', 'update']]);
        }
    }

    public function index() {
        return view('user.index');
    }

    public function json() {
        if ($this->user->username == 'superadmin') {
            return User::all();
        } else {
            return User::where('role', 'user')->get();
        }
    }

    public function create() {
        $data['ministry'] = null;
        $data['ministries'] = Ministry::lists('name', 'id');
        return view('user.create', $data);
    }

    public function store(UserRequest $request) {
        $user = $request->all();
        $user['date_of_birth'] = Carbon::parse($request->input('date_of_birth'));
        $user['password'] = bcrypt($request->password);
        $user['status'] = "on";
        $message['user'] = User::create($user);
        $message['password'] = $request->password;
        $message['title'] = 'CMS: Create New User';

        $this->send($message);


        return redirect('dash/user');
    }

    public function show(User $user) {
        if ($this->user->role == 'admin') {
            if ($user->role == $this->user->role && $this->user->id != $user->id) {
                return redirect()->route('auth.logout');
            }
        } else if ($this->user->id != $user->id) {
            return redirect()->route('auth.logout');
        }

        $data['user'] = $user;
        $data['ministry'] = null;
        $data['ministries'] = Ministry::lists('name', 'id');

        return view('user.show', $data);
    }

    public function edit(User $user) {
        if ($this->user->role == 'admin') {
            if ($user->role == $this->user->role && $this->user->id != $user->id) {
                return redirect()->route('auth.logout');
            }
        } else if ($this->user->id != $user->id) {
            return redirect()->route('auth.logout');
        }
        $data['user'] = $user;
        $data['ministry'] = $user->ministry->id;
        $data['ministries'] = Ministry::lists('name', 'id');
        return view('user.edit', $data);
    }

    public function update(UserRequest $request, User $user) {
        $data = $request->except(['password', 'password_confirmation']);
        $data['date_of_birth'] = Carbon::parse($request->input('date_of_birth'));
        if (!empty($request->password) || !empty($request->password_confirmation)) {
            $data['password'] = bcrypt($request->password);
        }
        $user->update($data);

        $message['user'] = $user;
        $message['password'] = $request->password;
        $message['title'] = 'CMS: Update Exists User';

        $this->send($message);

        if ($this->user->role == 'user') {
            return redirect('/');
        }

        return redirect('dash/user');
    }

    public function destroy(User $user) {
        $user->delete();
        if (\Request::ajax()) {
            return ['result' => true];
        }
        return redirect('dash/user');
    }
}
