<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request {
    public function authorize() {
        return true;
    }

    public function rules() {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];

            case 'POST':
                return [
                    'firstname'     => 'required',
                    'lastname'      => 'required',
                    'username'      => 'required|unique:users',
                    'date_of_birth' => 'required',
                    'password'      => 'required|confirmed',
                    'email'         => 'required|unique:users',
                    'role'          => 'required',
                    'gender'        => 'required'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'firstname' => 'required',
                    // 'username'  => 'required',
                    'password'  => 'confirmed',
                    // 'email'     => 'required'
                    // 'phone'     => 'required|regex:/[0-9]{9,13}/'
                ];
            default:
                break;
        }
    }

    public function messages() {
        return [
            'firstname.required'     => 'សូមបញ្ចូលឈ្មោះ!',
            'lastname.required'      => 'សូមបញ្ចូលនាមត្រកូល!',
            'username.required'      => 'សូមបញ្ចូលឈ្មោះគណនី!',
            'username.unique'        => 'ឈ្មោះគណនី ត្រូវបានគេយករួចហើយ!',
            'date_of_birth.required' => 'សូមជ្រើសយកភថ្ងៃខែ​ឆ្នាំ​កំណើត!',
            'password.required'      => 'សូមបញ្ចូលពាក្យសម្ងាត់!',
            'password.confirmed'     => 'ផ្ទៀងផ្ទាត់ មិនត្រូវគ្នា។!',
            'email.required'         => 'សូមបញ្ចូលអ៊ីម៉ែល!',
            'email.unique'           => 'អ៊ីម៉ែល ត្រូវបានគេយករួចហើយ!',
            'role.required'          => 'សូមជ្រើសយកតួនាទី!',
            'gender.required'        => 'សូមជ្រើសយកភេទ!'
        ];
    }
}
