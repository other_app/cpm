<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::group(['namespace' => 'Auth'], function () {
    Route::get('login', ['as' => 'auth.login', 'uses' => 'AuthController@getLogin']);
    Route::get('register', ['as' => 'auth.register', 'uses' => 'AuthController@postRegister']);
    Route::get('logout', ['as' => 'auth.logout', 'uses' => 'AuthController@getLogout']);

    Route::post('login', ['as' => 'auth.login', 'uses' => 'AuthController@postLogin']);
    Route::post('register', ['as' => 'auth.register', 'uses' => 'AuthController@getRegister']);

});

Route::get('/', ['as' => 'dash.budget.index', 'uses' => 'BudgetController@index', 'middleware' => 'auth']);

Route::group(['middleware' => ['auth'], 'prefix' => 'dash'], function () {
    Route::get('user/json', ['as' => 'dash.user.json', 'uses' => 'UserController@json']);
    Route::resource('user', 'UserController');
    Route::resource('ministry', 'MinistryController');

    Route::get('report_income', function () {
        return view('budget.report_income');
    });

    Route::get('report_expense', function () {
        return view('budget.report_expense');
    });

    // list
    Route::get('budget', ['as' => 'dash.budget.index', 'uses' => 'BudgetController@index']);
    Route::get('budget/json', ['as' => 'dash.budget.json', 'uses' => 'BudgetController@json']);

    // approve
    Route::get('budget/approve/{type}/{id}', ['as' => 'dash.budget.get_approve', 'uses' => 'BudgetController@getApprove']);
    Route::patch('budget/approve/{budget}', ['as' => 'dash.budget.patch_approved', 'uses' => 'BudgetController@patchApprove']);

    // create
    Route::get('budget/create/income', ['as' => 'dash.budget.create_income', 'uses' => 'BudgetController@create_income']);
    Route::get('budget/create/expense', ['as' => 'dash.budget.create_expense', 'uses' => 'BudgetController@create_expense']);

    // store
    Route::post('budget/create/income', ['as' => 'dash.budget.store_income', 'uses' => 'BudgetController@store_income']);
    Route::post('budget/create/expense', ['as' => 'dash.budget.store_expense', 'uses' => 'BudgetController@store_expense']);

    // edit
    Route::get('budget/income/{budget}/edit', ['as' => 'dash.budget.edit_income', 'uses' => 'BudgetController@edit_income']);
    Route::get('budget/expense/{budget}/edit', ['as' => 'dash.budget.edit_expense', 'uses' => 'BudgetController@edit_expense']);

    // update
    Route::patch('budget/income/{budget}', ['as' => 'dash.budget.update_income', 'uses' => 'BudgetController@update_income']);
    Route::patch('budget/expense/{budget}', ['as' => 'dash.budget.update_expense', 'uses' => 'BudgetController@update_expense']);
});
