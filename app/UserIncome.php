<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserIncome extends Model {
    protected $fillable = [
        'user_id',
        'ministry_id',
        'created_at',
        'chapter_70',
        'chapter_71',
        'chapter_72',
        'chapter_73',
        'chapter_74',
        'chapter_75',
        'chapter_76',
        'chapter_77',
        'chapter_78',
        'chapter_27',
        'chapter_50',
        'approved_id'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
