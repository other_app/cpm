<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model {
    public function adminIncome() {
        return $this->belongsTo('App\AdminIncome');
    }

    public static function income($fields = ['en_no', 'kh_no', 'income']) {
        return Chapter::select($fields)->where('income', '!=', 'null')->get();
    }

    public static function expense($fields = ['en_no', 'kh_no', 'expense']) {
        return Chapter::select($fields)->where('expense', '!=', 'null')->get();
    }
}
